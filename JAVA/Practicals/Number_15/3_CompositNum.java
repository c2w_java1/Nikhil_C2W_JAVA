/*
Q3 Write a program to check whether the given number is composite or
not.
Input: 6
Output: 6 is a composite number.
Input: 11
Output: 11 is not a composite number.
*/

import java.io.*;

class Composite{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Number : ");
		int num = Integer.parseInt(br.readLine());

		int count = 0;
		for(int i=2; i<num; i++){
		
			if(num%i==0){
			
				System.out.println(num +" is Composite Number.");
				count++;
				break;
			} 
		}

		if(count==0){
		
			System.out.println(num +" is not Composite Number.\n");
	
		}
		}
}
