/*
 * Q10 Write a program to check whether the given number is palindrome or
not.
Input : 57322375
Output : 57322375 is a palindrome number.
Input : 45698654
Output : 45698654 is not a palindrome number.
*/

import java.util.*;

class Palindrom10{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number : ");
		int num = sc.nextInt();

		int org = num;
		int rem =0;
		int temp=0;
		while(num>0){
		
			rem = num%10;
			temp =temp*10+rem;
		        num/=10;	
		}

		if(org==temp){
		
			System.out.println(org+" is a Palindrom Number.");
		} else {
			System.out.println(org+" is not a Palindrom Number.");                                                                            }

	}
}
