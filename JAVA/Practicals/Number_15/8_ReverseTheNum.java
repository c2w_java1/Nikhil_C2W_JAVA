/*
 * Q8 Write a program to reverse the given number.
Input : 93079224
Output : Reverse of 93079224 is 42297039.
*/

import java.util.*;

class ReverseTheNum2{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		int temp=0;

		System.out.print("Reverse of "+num +" is ");
		while(num>0){
		
			temp = num%10;

			System.out.print(temp);
			num/=10;
		}
		System.out.println();
	}
}
