/*
Write a program to print the factors of a given number.
Input: 12
Output: Factors of 12 are 1,2,3,4,6,12
*/

import java.io.*;

class Factors{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Number : ");
		int num = Integer.parseInt(br.readLine());

		System.out.print("Factors of '"+ num +"' are : ");
		for(int i=1; i<=num; i++){
		
			if(num%i==0){
			
				System.out.print(i +", ");
			}
		}
		System.out.println();

		
	}
}
