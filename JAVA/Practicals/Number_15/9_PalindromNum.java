/*
 * Q9 Write a program to check whether the given number is palindrome or
not.
Input : 12121
Output : 12121 is a palindrome number.
Input : 12345
Output : 12345 is not a palindrome number.
*/

import java.util.*;

class PalindromNum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		int temp=0;

		int original = num;
		int reverse = 0;

		while(num>0){
		
			temp = num%10;
			reverse = reverse*10 + temp;
			num/=10;
		}

		if(reverse==original){
		
			 System.out.println(original +" is a palindrom Number.");
		}else{

			System.out.println(original+" is not palindrom Number.");
	
		}
	}
}
