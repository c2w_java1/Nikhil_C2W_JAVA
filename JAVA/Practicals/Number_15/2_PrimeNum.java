/*
Q1 Write a program to check whether the given number is prime or not.
Input: 7
Output: 7 is a prime number.
Input: 12
Output: 12 is not a prime number.
*/

import java.util.*;

class PrimeNum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a Number : ");
		int num = sc.nextInt();

		
		for(int i=2; i<num; i++){
		
			if(num%i!=0){
			
				System.out.print(num +" is a Prime Number.");
				break;
			} else {
			
				 System.out.print(num +" is NOT a Prime Number.");
				 break;
			}
		}
		 System.out.println();
	}
}
