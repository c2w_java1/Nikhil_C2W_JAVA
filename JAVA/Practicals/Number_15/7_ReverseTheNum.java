/*
Q7 Write a program to reverse the given number.
Input : 7853
Output : Reverse of 7853 is 3587.
*/

import java.util.*;

class ReverseTheNum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();
		
		int temp=0;
		System.out.print("Reverse of "+num +" is ");
		while(num>0){
		
			temp=num%10;
			System.out.print(temp);
			num/=10;
		}
		System.out.println();

	}
}
