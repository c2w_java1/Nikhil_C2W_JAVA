/*
Q5 Write a program to print the factorial of the number.
Input : 5
Output : Factorial of 5 is 120.
*/

import java.io.*;

class Factorial{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Number : ");
		int num = Integer.parseInt(br.readLine());

		int factorial = 1;
		System.out.print("Factorial of "+num );
		while(num>0){
		
			factorial *=num;
		        num--;	
		}
		System.out.print(" = "+factorial);
		System.out.println();
	}
}
