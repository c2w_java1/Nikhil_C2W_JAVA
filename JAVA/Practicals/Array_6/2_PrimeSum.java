/*
 * Q2. Write a program to find out the sum of all prime numbers in an array and
also print the count of prime numbers in an array.
Example:

5 7 9 12 17 19 21 22

Output:
Sum of all prime numbers is 48 and count of prime numbers in the given array is 4
*/

import java.util.*;
class PrimeSum{
     public static void main(String n[]){

	Scanner sc = new Scanner(System.in);

	System.out.print("Enter Size : ");
	int size = sc.nextInt();

	int arr[] = new int[size];

	System.out.println("Enter Elements in Array : ");
	for(int i=0; i<arr.length; i++){
	
		arr[i] = sc.nextInt();
	}

	int sum=0,count=0;
	for(int i=0; i<arr.length; i++){
		
		int flag = 0;
		for(int j=2; j<arr[i]; j++){
		
			if(arr[i]%j==0){
			
				flag = 1;
				break;
			}
		}
		if(flag != 1 && arr[i] != 2){
			count++;
			sum += arr[i];
		}
	}

	System.out.println("Sum of all "+count+" Prime Number is = "+sum);
	}
}
