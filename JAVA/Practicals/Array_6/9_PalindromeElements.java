/*
 * Q9. Write a program to count the palindrome elements in your array.
arr
121 1 58 333 616 9

Count of palindrome elements is : 5

(Single number is also a palindrome number)
*/

import java.util.*;
class PalindromeElements{
	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
	
		int count = 0;
		System.out.print("Palindrome Elements in Array is ->  ");
		for(int i=0; i<arr.length; i++){
			
			int temp = arr[i];
			int rem = 0;
			int rev = 0;
			while(temp>0){
			
				rem = temp%10;
				rev = rev*10+rem;
				temp /=10;
			}
			
			if(arr[i]==rev){
				count++;
				System.out.print(arr[i]+", ");
			}
		}
		if(count==0){
		
			System.out.println("\nThere is NO Palindrome Element in Array.");
		} else {
			System.out.println("\nCount of palindrome elements is : "+count);
		}
	}
}
