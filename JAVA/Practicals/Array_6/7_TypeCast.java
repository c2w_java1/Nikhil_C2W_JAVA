/*
 * Q7. Write a program to, where you have to take integer input from user and if
the integer is in the range of ASCII value of A to Z, while saving you have to
type cast the int to char. Print the array.
Enter size :
7
Enter array elements :

55
67
65
97
36
13
68

Array will be like :
arr1
55 C B 97 36 13 D
*/

import java.util.*;

class TypeCast{

	public static void main(String n[]){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		char ch[] = new char[size];
		System.out.println("Enter Elements in Array ");
		for(int i=0; i<ch.length; i++){	
			ch[i]=(char)sc.nextInt();
		}

		System.out.println("Elements in Array ");
                for(int i=0; i<ch.length; i++){
			if(ch[i]>=65 && ch[i]<=90){
                              System.out.print(ch[i]+"  ");
                        }else{
				
				System.out.print((int)ch[i]+"  ");
			}
		}
	}
}
