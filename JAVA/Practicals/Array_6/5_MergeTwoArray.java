/*
 * Q5. Take two different arrays where size of array may differ, you have to
create an array by combining both the arrays (you have to merge the arrays)
Example :

arr1
5 10 15 20 25 30 35

arr2
4 8 12 16 20

Array after merger :

5 10 15 20 25 30 35 4 8 12 16 20
*/

import java.util.*;
class MergeArray{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array-1 : ");
		int size1 = sc.nextInt();

		int arr1[] = new int[size1];

		System.out.print("Enter Elements in Array-1 : ");
                for(int i=0; i<arr1.length; i++){
			arr1[i] = sc.nextInt();
		}

		System.out.print("Enter Size of Array-2 : ");
                int size2 = sc.nextInt();

		int arr2[] = new int[size2];
		
		System.out.print("Enter Elements in Array-2 : ");
		for(int i=0; i<arr2.length; i++){                                                                                                             arr2[i] = sc.nextInt();                                                                                                       }
		
		int mergeArr[] = new int[size1+size2];

		System.out.print("\nArray 1 -> ");
		for(int i=0; i<arr1.length; i++){
		
			System.out.print(arr1[i] +" ");
			mergeArr[i] = arr1[i];
		}

		System.out.print("\nArray 2 -> ");
		for(int i=0,j=arr1.length; i<arr2.length; i++,j++){
			System.out.print(arr2[i] +" ");	
			mergeArr[j] = arr2[i];
		}

		
		System.out.print("\nMerge Array --> ");
		for(int i=0; i<arr1.length+arr2.length; i++){
		
			System.out.print(mergeArr[i]+"  " );
		}
	}
}
