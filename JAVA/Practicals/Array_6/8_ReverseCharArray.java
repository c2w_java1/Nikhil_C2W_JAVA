/*
 * Q8. Write a program to reverse the char array and print the alternate
elements of the array before and after reverse.

Size = 10
A B C D E F G H I J

After reverse :

J I H G F E D C B A

Output :
Before Reverse:
A C E G I

After Reverse:

J H F D B
*/

import java.util.*;

class ReverseChar{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size : ");
		int size = sc.nextInt();

		char ch[] = new char[size];
		System.out.println("Enter Character Elements in Array : ");
		for(int i=0; i<ch.length; i++){
		
			ch[i]=sc.next().charAt(0);
		}

		System.out.print("\nArray Before Reverse : ");
		for(int i=0; i<ch.length; i++){
		
			System.out.print(ch[i]+" ");
		}

		System.out.print("\nAlternate Elements in Array Before Reverse : ");
                for(int i=0; i<ch.length; i+=2){

                        System.out.print(ch[i]+" ");
                }
			
		for(int i=0; i<ch.length/2; i++){
		
			char temp = ch[i];
                        ch[i]=ch[size-i-1];
                        ch[size-i-1]=temp;
		}
		System.out.print("\nArray After Reverse : ");
		for(int i=0; i<ch.length; i++){
			System.out.print(ch[i]+" ");
		}

		System.out.print("\nAlternate Elements in Array After Reverse : ");
                for(int i=0; i<ch.length; i+=2){

                        System.out.print(ch[i]+" ");
                }


	}
}
