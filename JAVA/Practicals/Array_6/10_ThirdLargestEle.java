/*
 * Q10. Write a program to find the third largest element in an array.
Example :
56 15 8 26 7 50 54

Output:
Third largest element is: 50
*/

import java.util.*;
class ThirdLargest{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Elements in Array :");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		int max1=0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]>max1){
				max1 = arr[i];
			}
		}
		
		int max2=0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]>max2 && arr[i]<max1){
				max2 = arr[i];
			}
		}


		int max3=0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]>max3 && arr[i]<max2 ){
				max3 = arr[i];
			}
		}

		System.out.println("Third Largest Element in Array : "+max3);
	}
}
