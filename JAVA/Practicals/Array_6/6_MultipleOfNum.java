/*
 * Q6. Write a program to check whether the array contains element multiple of
user given int value, if yes then print it’s index.
Example :
5 10 16 20 25 30 35 4 8 12 16 20
Input 1:
Enter key : 5
Output:
An element multiple of 5 found at index : 0
An element multiple of 5 found at index : 1
An element multiple of 5 found at index : 3
An element multiple of 5 found at index : 4
An element multiple of 5 found at index : 5
An element multiple of 5 found at index : 6
An element multiple of 5 found at index : 11
Input 2:
Enter key: 99
Output : Element Not Found.
*/

import java.util.*;
class Multiple{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[]= new int[size];
		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Enter Number : ");
                int num = sc.nextInt(); 

		int flag = 0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]%num==0){
				flag = 1;
				System.out.println("Element Multiple of "+num +" is found at Index : "+i);
			}
		}
		if(flag == 0){
			System.out.println("Element Multiple of "+num+" NOT found in Array ");
		}
	}
}
