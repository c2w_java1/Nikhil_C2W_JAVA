/*
 * Q3. Write a program to check the count of the user given key in your array, if
the count is more than 2 times replace the element with its cube, print the
array.
Example:
arr1:
11 6 8 9 5 8 7 8 6

Input 1:
Enter key: 8
Output
Array will be like :
11 6 512 9 5 512 7 512 6

Input 2:
Enter key: 12
Output : Element Not Found.
*/

import java.util.*;

class CubeOfKey{

	public static void main (String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Enter Key : ");
		int key = sc.nextInt();
		int count = 0;
		for(int i=0; i<arr.length; i++){
			
			if(arr[i]==key){
			
				count++;
			}
		}

               	for(int i=0; i<arr.length; i++){
			if(count>2){
    		                if(arr[i]==key){
	                                arr[i] = key*key*key;
                                }
				System.out.print(arr[i] +"  ");
                        }
                }
		if(count >0 && count<=2){

                        System.out.println("Element Found "+count +" times in Array, Hence No change Occured in Array ");
                } else if(count == 0){
                                System.out.print("Elements not Found in Array ");
                }
	}
}
