/*
 * Q4
Write a program to convert all even numbers into 0 and odd numbers into 1 in a given
array.
1 2 3 12 15 6 7 10 9
Output:

1 0 1 0 1 0 1 0 1
*/

import java.util.*;

class Convert{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size Of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Element : ");
		for(int i=0; i<size; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Array : ");
		for(int i=0; i<size; i++){
		
			System.out.print(arr[i] +"  ");
		}

		System.out.println();
		System.out.print("Array After Squre :: ");
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]%2==0){
			
				arr[i] = 0;
			} else {
			
				arr[i] = 1;
			}
			
			System.out.print(arr[i] +"  ");
			
		}
		System.out.println();

		

	}
}
