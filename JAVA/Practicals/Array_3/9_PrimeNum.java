/*
 * Q9.
Print the prime numbers in an array.
5 7 9 11 15 19 90
Output:
5 7 11 19
*/

import java.util.*;

class PrimeNum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Element in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.println();
		System.out.print("Prime Numbers in Array : ");
		
		for(int i=0; i<arr.length; i++){
		
			int flag = 0;
			for(int j=2; j<arr[i]; j++){
			

				if(arr[i]%j==0){

					flag = 1;
					break;
				} 
			}

			if(flag==0 && arr[i]!=2){
			
				System.out.print(arr[i]+"  ");
			}
			
		}
		System.out.println();
	}
}
