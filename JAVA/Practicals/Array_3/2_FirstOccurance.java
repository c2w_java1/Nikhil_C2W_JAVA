/*
 * Q2
Write a program to find the first occurrences of a specific number in an array. Print the
index of a first occurrence.
Example :
1 5 9 8 7 6
Input: Specific number : 5
Output: num 5 first occurred at index : 1
Input: Specific number : 11
Output: num 11 not found in array.
*/

import java.util.*;

class FirstOccurance{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		
		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Enter Specific Element : ");
		int sp = sc.nextInt();

		System.out.print("Array :: ");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] +"\t");
		}

		System.out.println();
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]==sp){
			
				System.out.println("First Occurance of "+sp +" is at Index - "+i);
			}
		}
		
	}
}
