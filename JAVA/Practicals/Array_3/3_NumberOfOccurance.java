/*
 * Q3
Write a program to find the number of occurrences of a specific number in an array. Print
the count of occurrences.
2 5 2 7 8 9 2
Input Specific number: 2
Output : Number 2 occurred 3 times in an array.
Input: Specific number : 11
Output: num 11 not found in array.
*/

import java.util.*;

class NumberOfOccurance{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Element in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Array :: ");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i]+"  ");
		}

		System.out.println();
		System.out.print("Enter Specific Element : ");
		int sp = sc.nextInt();

		int count = 0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i] == sp){
			
				count++;
			}
		}

		if(count == 0){
			
			System.out.println(sp +" is NOT Occured in Array.");
		} else {
		
			System.out.println(sp +" is Occured '"+count +"' Times in Array.");
		}
	}
}
