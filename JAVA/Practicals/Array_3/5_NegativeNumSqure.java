/*
 * Q5
Write a program to convert all negative numbers into their squares in a given array.

-2 5 -6 7 -3 8

Output:
4 5 36 7 9 8
*/

import java.util.*;

class Negative{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size Of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Element : ");
		for(int i=0; i<size; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Array : ");
		for(int i=0; i<size; i++){
		
			System.out.print(arr[i] +"  ");
		}

		System.out.println();
		System.out.print("Array After Squre :: ");
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]<0){
			
				arr[i] *= arr[i];
			} 
			
			System.out.print(arr[i] +"  ");
			
		}
		System.out.println();

		

	}
}
