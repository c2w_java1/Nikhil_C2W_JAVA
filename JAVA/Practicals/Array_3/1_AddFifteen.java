/*
 Q1
Write a program to add 15 in all elements of the array and print it.
10 20 30 40 50 60
Output:
25 35 45 55 65 75
*/

import java.util.*;

class Addition{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements in Array : ");
		
		for(int i=0; i<arr.length; i++){
		
			arr[i]=sc.nextInt();
			
		}

		System.out.print("Element in array : ");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i]+"\t");
			arr[i] += 15;
		}

		System.out.println();
		System.out.print("Element in array After adding 15 in each : ");
		for(int i=0; i<arr.length; i++){                                                                                                                                                                                                                                                   System.out.print(arr[i]+"\t");                                                                                                  }
		System.out.println();
	}
}
