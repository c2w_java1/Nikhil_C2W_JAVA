/*
 * Q10.
Write a program to print the product of prime elements in an array.
1 4 5 6 11 9 10
Output:
55
*/

import java.util.*;

class ProductOfPrime{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		int prod = 1;
		System.out.println();
		System.out.print("Prime Numbers in Array :: ");
		
		for(int i=0; i<arr.length; i++){
		
			int flag = 0;
			for(int j=2; j<arr[i]; j++){
			
				if(arr[i]%j==0){
				
					flag = 1;
					break;
				}
			}

			if(flag == 0 && arr[i]!=2){
			
				System.out.print(arr[i] +"  ");
				prod *= arr[i];
			}
		}
		System.out.println();
		System.out.println("Product of Prime Numbers -> "+prod);
	}
}
