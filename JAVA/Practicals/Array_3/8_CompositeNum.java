/*
 * Q8.
Print the composite numbers in an array.
4 5 7 9 10

Output:
4 9 10
*/

import java.util.*;

class Composite{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Composite Number in Array -> ");
		for(int i=0; i<arr.length; i++){
		
			for(int j=2; j<arr[i]; j++){
			
				if(arr[i]==1 || arr[i]%j==0){
				
					System.out.print(arr[i]+"  ");
					break;
				}
			}
			
		}
		System.out.println();
	}
}
