/*
 * Q6
Write a program to print all the consonants in an array.
a b c y x o p
Output:
b c y x p
*/

import java.util.*;

class Consonant{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		System.out.println("Enter Character in Array is : ");
		char arr[] = new char[size];
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.next().charAt(0);
		}
		
		System.out.print("Consonat in Array is -> ");
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]!='a' && arr[i]!='e' && arr[i]!='i' && arr[i]!='o' && arr[i]!='u' && arr[i]!='A' && arr[i]!='E' && arr[i]!='I' && arr[i]!='O' && arr[i]!='U'){
			
				System.out.print(arr[i] +"  ");
			}
		}
		System.out.println();
	}
}
