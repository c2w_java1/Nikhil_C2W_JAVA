/*
 * Q7
Check the size of the array and if array size is odd and greater than or equal to 5, then
print the odd elements, else print the even numbers.
Example 1:
Size : 6
121 144 225 88 90 89
Output:
144 88 90
*/

import java.io.*;

class Size{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Size of Array : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter Elements in Array : ");
	
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.print("Output -> ");
		for(int i=0; i<arr.length; i++){
		
			if(size%2==1 && size<=5){
			
				if(arr[i]%2==1){
				
					System.out.print(arr[i]+"  ");
				}
			}else {
			
				if(arr[i]%2==0){
				
					System.out.print(arr[i]+"  ");
				}

			}
		}
	}
}
