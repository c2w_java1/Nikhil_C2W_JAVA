/*
9.Dry run the given code in your notebook where you have to check is there any error if
yes mention so also if there is no error write the output.
*/

class WhileDemo4{


	public static void main(String[] args){

		int num =10;

		while(num!=0){

			num=num-1;
			int temp;
			temp*=num;

			System.out.println(temp);

		}

		System.out.println(num);
	}
}

/*output--
 
  9_WhileDemo.java:17: error: variable temp might not have been initialized
                        temp*=num;
                        ^
1 error
 */
