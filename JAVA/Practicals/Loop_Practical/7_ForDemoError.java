/*
7.Dry run the given code in your notebook where you have to check is there any error if
yes mention so also if there is no error write the output.

*/

class SkipVowels{

	public static void main(String[] args){

		for(char ch =65;ch<91;++ch){

			if(ch=='A'||ch=='O'||ch=='I'||ch=='E'||ch=='U'){

				continue;
			}

			System.out.println(ch);
		}
	}
}
