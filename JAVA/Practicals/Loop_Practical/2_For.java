/*
2.Dry run the given code in your notebook where you have to check is there any error if
yes mention so also if there is no error write the output.
*/

class NumFor{

	public static void main(String... args){
	
		int num = 23456789;

		for( ; num!=0;num/=10 ){
			
			int temp = num%10;
			
			if(temp%2==0){

				System.out.println(temp*temp+" ");
			}

		}
	}
}
