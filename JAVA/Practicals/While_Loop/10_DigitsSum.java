/*
10.Write a program to print the sum of digits in the given number.
Input: 9307922405
Output: sum of digits in 9307922405 is 41
*/

class Sum{

	public static void main(String n[]){
	
		long num = 9307922405l;
		long sum = 0;

		while(num>0){

			long rem = num%10;
			sum = sum + rem;

			num /=10;
			
		}
		System.out.println("Sum of Digits in " +num +" is => "+ sum);
	}
}
