/*
5. Row= 4
1 3 5 7
9 11 13 15
17 19 21 23
25 27 29 31
*/

class ForeRow{

	public static void main(String n[]){
	
		int row = 1;
		int num = 1;
         
		while(row <= 4){

			int column = 1;
			while(column <= 4){
			
				System.out.print(num+ "   ");
				num+=2;

				column++;
			}
			row++;
			System.out.println();
		}
	}
}
