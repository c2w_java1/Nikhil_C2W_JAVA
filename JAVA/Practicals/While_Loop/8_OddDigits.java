/*
8. Write a program to print the odd digits of a given number.
Input : 216985
Output : 5 9 1
*/

class OddDigits{

	public static void main(String n[]){
	
		int num = 216985;
		
		while (num > 0){
			
			int rem = num % 10;
			num/=10;

			if(rem%2 == 1){
			
				System.out.print(rem +" ");
			}
			
		}
		System.out.println();

	}
}
