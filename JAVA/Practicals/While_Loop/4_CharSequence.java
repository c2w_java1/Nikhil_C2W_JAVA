/*
4. Write a program to print the character sequence given below where the
input: start=1, end =6
This means iterate the loop from 1 to 6
Output: A 2 C 4 E 6 (if num is odd print the character)
*/

class CharSequence{

	public static void main(String n[]){
	
		int num = 1;
		char ch = 'A';
		while(num <= 6){
		
			if(num%2==0){
			
				System.out.print(num +" ");
			}else {
			
				System.out.print(ch +" ");
			}
			ch++;
			num++;
		}
		System.out.println();

	} 
}
