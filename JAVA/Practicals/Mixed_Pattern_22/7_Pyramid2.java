/*
7.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    1
  3 2 1
5 4 3 2 1
row=4

      1
    3 2 1
  5 4 3 2 1
7 6 5 4 3 2 1
*/
import java.io.*;

class Pyramid2{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int rows = Integer.parseInt(br.readLine());

		
		for(int i=1; i<=rows; i++){
		
			for(int space=rows; space>i; space--){
			
				System.out.print(" \t");
			}

			int num = i*2-1;
			for(int j=1; j<=i*2-1; j++){
			
				System.out.print(num-- +"\t");
				
			}
			System.out.println("\n");
		}
		
	}
}
