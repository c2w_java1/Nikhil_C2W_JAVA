/*
8.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    3
  2 3 4
1 2 3 4 5

row=4

      4
    3 4 5
  2 3 4 5 6
1 2 3 4 5 6 7
*/

import java.io.*;

class Pyramid8{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			int num = 1;

			for(int space=rows; space>i; space--){
			
				System.out.print(" \t");
				num++;
			}

			for(int j=1; j<=i*2-1; j++){
				
				System.out.print(num +"\t");
				num++;
			}
			System.out.println("\n");
		}
	}
}
