/*
4. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
C 4 5
F 7 8
I 10 11

row=4
D 5 6 7
H 9 10 11
L 13 14 15
P 17 18 19
*/

import java.io.*;

class SquarePattern4{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int ch = 64+rows;
		int num = rows;

		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=rows; j++){
			
				if(j==1){
				
					System.out.print((char)ch +"  ");
				} else {
				
					System.out.print(num +"  ");
				}
				num++;
				ch++;
			}
			System.out.println("\n");
		}


	}
}
