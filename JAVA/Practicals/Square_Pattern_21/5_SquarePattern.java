/*
 5. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
3 16 5
36 7 64
9 100 11
row=4
16 5 36 7
64 9 100 11
144 13 196 15
256 17 324 19
*/

import java.io.*;

class SquarePattern5{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = rows;

		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=rows; j++){
			
				if(num%2==0){
				
					System.out.print(num*num +"  ");
				} else {
				
					System.out.print(num +"  ");
				}

				num++;
			}
			System.out.println(" \n ");
		}
	}
}
