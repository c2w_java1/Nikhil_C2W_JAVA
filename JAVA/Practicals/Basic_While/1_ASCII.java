/*
1. Write a program to print the character for ASCII value in range 90 - 65
*/

class CharacterDemo{

	public static void main(String n[]){
	
		int num = 90;

		while(num >= 65){
		
			System.out.println((char)num);
			num--;
		}
	}
}
