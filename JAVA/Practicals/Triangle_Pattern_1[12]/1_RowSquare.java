/*
1. Write a program to print the given pattern
rows=3
9
9 9
9 9 9

rows=4
16
16 16
16 16 16
16 16 16 16
*/

import java.io.*;

class Square{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter No. Of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=i; j++){
			
				System.out.print(rows*rows +"  ");
			}
			System.out.println("\n");
		}

	}
}
