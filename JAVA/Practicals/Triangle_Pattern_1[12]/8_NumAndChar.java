/*
8. Write a program to print the given pattern
rows=3
1
1 c
1 e 3

rows=4
1
1 c
1 e 3
1 g 3 i
*/

import java.io.*;

class NumAnd{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of Rows : ");
		int rows = Integer.parseInt(br.readLine());


		char ch = 'a';
		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=i; j++){
			
				if(j%2==0){
				
					System.out.print((char)ch +"  ");
					
				} else {
				
					System.out.print(j +"  ");
				}
				ch++;
			
			}
			System.out.println("\n");
		}
	}
}
