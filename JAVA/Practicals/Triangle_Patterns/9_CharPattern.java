/*
9. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
D C B A
D C B
D C
D

Rows = 5
E D C B A
E D C B
E D C
E D
E
*/

import java.io.*;

class CharPattern{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("How Many Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1 ; i<=rows ; i++){
		
			int ch = 64+rows;
			for(int j=rows ; j>=i ; j--){
			
				System.out.print((char)ch +"   ");
				ch--;
			}
			System.out.println("\n");
		}
	}
}
