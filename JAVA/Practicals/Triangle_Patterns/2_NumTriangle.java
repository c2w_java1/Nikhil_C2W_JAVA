/*
2. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
3
3 2

3 2 1

Rows = 4
4
4 3
4 3 2
4 3 2 1
*/

import java.io.*;

class NumTriangle2{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i<=row ; i++){
		
			int num = row;
			for(int j=1; j<=i; j++){
			
				System.out.print(num +"  ");
				num--;
			}

			System.out.println("\n");
		}
	}
}
