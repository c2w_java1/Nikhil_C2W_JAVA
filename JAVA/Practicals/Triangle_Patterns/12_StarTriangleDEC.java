/*
Take number of rows from user
Row=4
* * * *
* * *
* *
*
*
*/

import java.io.*;

class StarDec{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
		
			for(int j=rows; j>=i; j--){
			
				System.out.print(" *   ");
			}
			System.out.println("\n");
		}
	}
}
