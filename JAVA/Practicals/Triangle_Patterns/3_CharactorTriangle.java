
/*
3. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
A
B C
C D E
Rows = 4
A
B C
C D E
D E F G
*/


import java.io.*;

class CharatorTriangle{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i<=row ; i++){

			int ch = 64+i;

			for(int j=1; j<=i; j++){
			
				System.out.print((char)ch +"  ");
				ch++;
			}
			System.out.println("\n");

		}
	}
}
