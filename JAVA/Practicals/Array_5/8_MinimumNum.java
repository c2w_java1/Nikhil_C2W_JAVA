/*
 * Q8. WAP to find the second minimum element in an array.
Example:
Input:
Enter the size:5
Enter the elements of the array:
10
2
31

4
0
Output:
The second largest element in the array is: 2
*/

import java.util.*;
class SecondMin{
	
	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the Size of array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter the elements :");

		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		int min = arr[0];
		
		for(int i=0; i<arr.length; i++){
			
			if(arr[i]<min){
				min = arr[i];
			}
		}
		
		int sMin = arr[0];

		for(int i=0; i<arr.length; i++){
			
			if(arr[i] <= sMin && arr[i] > min){
				sMin = arr[i];
			}
		
		}
		System.out.println("\nMinimum element in Array -> " + min);
		System.out.println("\nSecond Minimum element in Array -> " + sMin);
	
	}

}
