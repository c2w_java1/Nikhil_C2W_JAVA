/*
 * Q7. WAP to find the composite numbers in an array.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
10
22
3
31
50
3

Output:
Composite numbers in an array are: 10, 22, 50
*/
import java.util.*;

class Composite{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Array Elements :");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Composite Numbers in Array : ");

		for(int i=0; i<arr.length; i++){
		
			int flag = 0;
			for(int j=2; j<arr[i]; j++){

				if(arr[i]%j==0 || arr[i]==2){
				
					flag = 1;
					break;
				}
			}
			if(flag == 1 || arr[i]==2){
			
				System.out.print(arr[i]+", ");
                                
			}
		}
	}
}
