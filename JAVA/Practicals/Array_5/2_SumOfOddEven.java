/*
 * Q2. WAP to print the sum of odd and even numbers in an array.
Enter the size of the array:
6
Enter the elements of the array:
10
15
9
1
12
15
Output:
Odd Sum = 40
Even Sum = 22
*/

import java.util.*;

class Sum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		int oSum = 0, eSum = 0;
		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();

			if(arr[i]%2 == 0){
			
				eSum += arr[i];
			} else {
			
				oSum +=arr[i];
			}
		}

		System.out.println("Sum of 'Even' Elements in Array = "+eSum +"\nSum of 'Odd' Elements in Array = "+oSum);
	}
}
