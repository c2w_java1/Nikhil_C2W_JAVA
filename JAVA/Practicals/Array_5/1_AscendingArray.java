/*
 * Q1. WAP to check whether the array is in ascending order or not.
Input 1:
Enter the size of the array:
4
Enter the elements of the array:
1
5
9
15
Output 1:
The given array is in ascending Order.
Input 2 :
Enter the size of the array:
4
Enter the elements of the array:
1
5
9
7
Output 2 :
The given array is not in ascending Order.
*/

import java.io.*;

class AscendingArray{

	public static void main(String n[]) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Size of Array : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Element in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.print("Array -> ");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] +"  ");
		}

		int flag = 0 ;
		for(int i=0; i<arr.length-1; i++){
		
			if(arr[i]>arr[i+1]){
			
				System.out.println("\nGiven Array is NOT in Ascending Order.");
				flag = 1;
				break;
			}
		}
		
		if(flag == 0){
		
			System.out.println("\nGiven Array is in Ascending Order.");
		}
	}
}
