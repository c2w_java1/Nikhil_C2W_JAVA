/*
 * Q5. WAP to print the count of digits in elements of an array.
Example :
Input:
Enter the size of the array:
4
Enter the elements of the array:
1
225
32
356

Output:
1, 3, 2, 3
*/

import java.util.*;

class CountOfDigits{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		for(int i=0; i<arr.length; i++){
		
			int temp = arr[i];
			int count = 0;
			
			while(temp>0){
			
				count++;
				temp /=10;
			}

			System.out.println(count+" Digits in Element ("+arr[i]+") at Index "+i);
		}
	}
}
