/*
 * Q3. WAP to check if an array is a palindrome or not .
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
3
2
1
Output:
The given array is a palindrome array.
*/

import java.util.*;

class Palindrome{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in); 

		System.out.print("Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter array Elements : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		int flag = 0;
		for(int i=0; i<arr.length/2; i++){
		
			if(arr[i]==arr[size-i-1]){
				
			}else{
				flag = 1;
				break;
			}
		}
		if(flag == 0){
		
			System.out.println("\n Given Areay is Palindrome.");
		}else{
		
			System.out.println("\n Given Areay is NOT Palindrome.");
		}
	}
}
