/*
 * Q9. WAP to take a number from the user and store each element in an array by increasing
the element by one.
Example:
Input:
Enter the Number:
1569872365
Output:
2, 6, 7, 10, 9, 8, 3, 4, 7, 6
Explanation : Each digit in a number is increased by one and stored in an array
*/

import java.util.*;
class Increament{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		int count = 0;
		int temp = num;
		while(num>0){
			count++;
			num /= 10;
		}

		int arr[] = new int[count];
		
		while(temp>0){
			arr[--count] = (temp%10) + 1;
			temp /=10;
		}

		System.out.print("Array :  ");
		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i]+"  ");
		}
	}
}
