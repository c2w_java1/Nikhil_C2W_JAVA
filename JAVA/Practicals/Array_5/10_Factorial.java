/*
 * Q10. WAP to print the factorial of each element in an array.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
5
8
2

Output:
1, 2, 6, 120, 40320, 2
*/
import java.util.*;

class Factorial{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Elements : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Factorial : ");
		for(int i=0; i<arr.length; i++){
		
			int factorial = 1;
			int temp = arr[i];
			while(temp>0){
			
				factorial *=temp;
				temp--;
			}

			System.out.print(factorial+", ");
		}
	}
}
