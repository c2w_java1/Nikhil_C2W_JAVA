/*
 *Q6. WAP to find the first prime number in an array.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
10
22
3
31
50
3
Output:
First prime number found at index 2
*/

import java.util.*;

class PrimeNum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();

		int arr[] = new int[size]; 
		System.out.println("Enter Array Elements : ");
		for(int i=0; i<arr.length; i++){
			
			arr[i] = sc.nextInt();
		}

		for(int i=0; i<arr.length; i++){
			
			int flag = 0;
			for(int j=2; j<arr[i]; j++){
			
				if(arr[i]%j==0){

					flag = 1;
					break;
				} 
			}
			if(flag == 0 && arr[i] != 2){
			
				System.out.print(arr[i]+" is First Prime Number in Array at Index "+i);
				break;
			}
		}
	}
}
