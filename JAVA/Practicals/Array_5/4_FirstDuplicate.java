/*
 * Q4. WAP to check the first duplicate element in an array and return its index.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
3
2
5
Output:
First duplicate element present at index 1
*/

import java.util.*;
class FirstDuplicate{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		int flag = 0;
		for(int i=0; i<arr.length; i++){
		
			for(int j=i+1; j<arr.length; j++){
			
				if(arr[j]==arr[i]){
				
					flag = 1;
					System.out.println("First Duplicate Element is "+arr[i]+" & Found at Index "+i);
					break;
				}
			}
			if(flag == 1){
				break;
			} 
		}
		if(flag==0){
		
			System.out.println("Duplicate Element NOT found in given Array");
		}
	}
}
