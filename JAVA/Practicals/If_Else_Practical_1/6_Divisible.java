/*
 6. Write a program to check whether the given number is divisible by 3 or 7.
Input: 15
Output: 15 is divisible by 3

Input: 28
Output: 28 is divisible by 7
Input: 20
Output: 20 is neither divisible by 3 nor by 7.
*/

class Divisible2{

	public static void main(String n[]){
	
		//int num = 15;
		int num = 28;
		//int num = 20;
		//int num = 21;

		if (num%3==0 || num%7==0){
			
			if(num%3==0 && num%7==0){

                        	System.out.println(num +" is Divisible by 3 & 7.");
               		} else if(num%3==0) {

				System.out.println(num +" is Divisible by 3.");
			} else {
			
				 System.out.println(num +" Divisible by 7.");
			}

		} else {
		
			System.out.println(num +" is Nither Divisible by 3 Nor by 7.");	
		}
	}
}
