/*
1. Write a program to check whether the given number is positive or not.(take hardcoded
values)
Input: num = 5;
Output: 5 is a positive number.
Input: num = -9;
Output: -9 is a negative number.

*/

class PositiveNegative{

	public static void main(String n[]){
	
		//int num = 5;
		int num = -9;

		if (num > 0){
		
			System.out.println(num +" is Positive Number.");
		} else {
		
			System.out.println(num +" is Negative Number.");
		}			
	}
}
