/*
 * Q10.Write a program to print the characters in an array which comes before the user
given character.
Output:
Input:
Enter the size:
6
Enter Elements
A
B
Y
G
H
P
Enter character key :
H
Output:
Array:
A
B
Y
G
*/

import java.util.*;

class BeforeUserChar{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		char ch[] = new char[size];
		System.out.println("Enter Array Element : ");

		for(int i=0; i<ch.length; i++){
		
			ch[i] = sc.next().charAt(0);
		}

		System.out.print("Enter Character : ");
		char c = sc.next().charAt(0);

		System.out.print("Array Upto User Character : ");
		for(int i=0; i<ch.length; i++){
		
			if(ch[i]==c){
			
				break;
			} else {
			
				System.out.print(ch[i]+"  ");
			}
		}
		System.out.println();
	}

}
