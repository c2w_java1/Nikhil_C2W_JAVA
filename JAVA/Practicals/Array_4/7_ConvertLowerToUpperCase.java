/*
 * Q7. WAP to convert lowercase characters to UPPERCASE characters.(Take input from
the user)
Example:
Input:
Enter the size of the array:
6
Enter the elements of the array:
a
B
Y
p
o
H
Output:
A B Y P O H
 * */


import java.util.*;

class Conversion{

        public static void main(String n[]){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Size of Array : ");
                int size = sc.nextInt();

                char ch[] = new char[size];

                System.out.println("Enter Array Elements : ");
                for(int i=0; i<ch.length; i++){

                        ch[i] = sc.next().charAt(0);
                }

		System.out.print("Array ->");
                for(int i=0; i<ch.length; i++){
			System.out.print(ch[i] +"  ");
                }

		System.out.println();
		System.out.print("Array After Conversion-> ");
		for(int i=0; i<ch.length; i++){
                        
			if(ch[i]>=97 && ch[i]<=122){			
				int temp = (int)ch[i]-32;
				ch[i]=(char)temp;
			}
			System.out.print(ch[i]+"  ");
                
		}
                
        }
}
