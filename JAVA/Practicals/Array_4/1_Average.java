/*
 * Q1.WAP to take input from the user for size and elements of an array, where you have to
print the average of array elements(Array should be of integers).
Example :
Input:
Enter the size:
4
Enter array elements:
2
4
6
8
Output:
Array elements' average is :5
*/

import java.util.*;

class Average{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		int sum = 0;
	
		for(int i=0; i<arr.length; i++){
	
			sum +=arr[i];
		}
		int avg = sum/arr.length;

		System.out.println("Average of Array Elements = "+avg);

	}
}
