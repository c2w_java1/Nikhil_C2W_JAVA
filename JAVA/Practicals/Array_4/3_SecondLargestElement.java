/*
 * Q3. WAP to find the second largest element in an array.
Example:
Input:
Enter the size :4
Enter the elements of the array:
1
2
3
4
Output:
The second largest element in the array is: 3
*/

import java.util.*;

class SecondLargest{

        public static void main(String n[]){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Size of Array : ");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter Array Elements : ");
                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		int max = arr[0];
		int sMax = arr[0];
                for(int i=0; i<arr.length; i++){

			if(arr[i]>max){
			
				max = arr[i];
			}
                }

                for(int i=0; i<arr.length; i++){

			if(arr[i]>sMax && arr[i]<max){
			
				sMax = arr[i];
			}
		}
                System.out.println("Second Largest Element in Array is -> "+sMax);
        }
}
