/*
 * Q6. WAP to count the vowels and consonants in the given array(Take input from the user)
Example:
Enter the size of the array:
6
Enter the elements of the array:
a
E
P
o
U
G
Output:
Count of vowels: 4
Count of consonants: 2
*/


import java.util.*;

class CountVowelConsonant{

        public static void main(String n[]){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Size of Array : ");
                int size = sc.nextInt();

                char ch[] = new char[size];

                System.out.println("Enter Array Elements : ");
                for(int i=0; i<ch.length; i++){

                        ch[i] = sc.next().charAt(0);
                }

		System.out.print("Array ->");
                for(int i=0; i<ch.length; i++){
			System.out.print(ch[i] +"  ");
                }

		int vCount = 0, cCount = 0;
		for(int i=0; i<ch.length; i++){
                        
			if(ch[i]=='a' || ch[i]=='e' || ch[i]=='i' || ch[i]=='o' || ch[i]=='u' || ch[i]=='A' || ch[i]=='E' || ch[i]=='I' || ch[i]=='O' || ch[i]=='U'){
			
				vCount++;
			} else {
			
				cCount++;
			}
                
		}

		System.out.println("\nCount of Vowels = "+vCount);
		System.out.println("Count of Consonants = "+cCount);

                
        }
}
