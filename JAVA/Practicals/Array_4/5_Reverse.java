/*
 * Q5. WAP to reverse the array(take input from the user).
Example:
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
4
5
6
Output:
Reversed array:
6
5
4
3
2
1
*/


import java.util.*;

class Reverse{

        public static void main(String n[]){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Size of Array : ");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter Array Elements : ");
                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		System.out.print("Before Reverse the Array : ");
		for(int i=0; i<arr.length; i++){
			 System.out.print(arr[i]+"  ");
                }

		int temp = 0;
                for(int i=0; i<arr.length/2; i++){
			temp = arr[i];
			arr[i] = arr[size-i-1];
			arr[size-i-1] = temp;
                }

		System.out.println();
		System.out.print("After Reverse the Array : ");	
		for(int i=0; i<arr.length; i++){
			System.out.print(arr[i]+"  ");
                }
		System.out.println();
                
        }
}
