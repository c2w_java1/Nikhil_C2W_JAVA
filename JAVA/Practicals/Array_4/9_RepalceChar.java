/*
 * Q9. WAP to replace the elements with #, which are not in the range of ‘a to z’.
Input:
Enter the size:
6
Enter Elements
A
B
c
d
E
*
Output
Array:
#

#
c
d
#
#
*/

import java.util.*;

class ReplaceChar{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		char ch[] = new char[size];

		System.out.println("Enetr Array Elements : ");
		for(int i=0; i<ch.length; i++){
		
			ch[i] = sc.next().charAt(0);
		}

		System.out.print("Array -> ");
		for(int i=0; i<ch.length; i++){
		
			System.out.print(ch[i]+"  ");
			
			if(ch[i]>=97 && ch[i]<=122){
			
			} else {
			 
				ch[i]='#';
			}
		}

		System.out.print("\nArray After Replace : ");
		for(int i=0; i<ch.length; i++){
		
			System.out.print(ch[i]+"  ");
		}
	}
}
