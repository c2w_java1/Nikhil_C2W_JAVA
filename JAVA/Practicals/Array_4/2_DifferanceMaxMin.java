/*
 * Q2.WAP to find the difference between minimum element in an array and maximum
element in an array, take input from the user.
Example :
Input:
Enter the size :
5
Enter the elements of the array:
3
6
9
8
10
Output
The difference between the minimum and maximum elements is: 7
*/

import java.util.*;

class Difference{

        public static void main(String n[]){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Size of Array : ");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter Array Elements : ");
                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		int max = arr[0], min = arr[0];

                for(int i=0; i<arr.length; i++){

			if(arr[i]<min){
				
				min = arr[i];
			} 
			
			if(arr[i]>max){
			
				max = arr[i];
			}
                }
                
		int diff = max - min;

		System.out.println("Diffetrence between Maximum & Minumum Element in Array is -> "+diff);
        }
}
