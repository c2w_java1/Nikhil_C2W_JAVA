/*
 * Q8. WAP to print the occurrence of a user given character.
Example:
Input:
Enter the size of the array:
5
Enter the elements of the array:
A
Y
U
O
U
P
Enter the character to check:
U
Output :
U occurs 2 times in the given array.
*/

import java.util.*;

class CharOccurance{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		char arr[] = new char[size];
		System.out.println("Enter Character Elements in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.next().charAt(0);
		}

		System.out.print("Enter Character : ");
		char ch = sc.next().charAt(0);

		int count = 0;
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]==ch){
			
				count++;
			}
		}

		if(count==0){

			System.out.println("\n"+ch +" is NOT found in Array.");
		} else {
		
			System.out.println(ch +" is Found "+count +" Times in Array.");
		}

	}
}
