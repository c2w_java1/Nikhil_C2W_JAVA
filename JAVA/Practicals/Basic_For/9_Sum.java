/*
9. Write a program to print the sum of the first 10 natural numbers.
Output:55

*/

class Sum{

	public static  void main(String n[]){
	
		int sum = 0;

		for (int i=1 ; i<=10 ; i++){
		
			sum +=i;
		}

		System.out.println("Sum of 1st 10 Natural Numbers :\n"+sum);
	}
}
