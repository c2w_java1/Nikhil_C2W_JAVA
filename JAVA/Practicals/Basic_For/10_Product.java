/*
 10. Write a program to print the product of the first 10 natural numbers.
Output: 3628800
*/

class Product{

	public static void main (String n[]){
	
		int product = 1;

		for (int i=1 ; i<=10 ; i++){
		
			product *= i;
		}
		
		System.out.println("Product of First 10 Natural Number :\n"+product);
	}

}
