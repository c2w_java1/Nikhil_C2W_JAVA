/*
8. Write a program to print a table of 14 in reverse order from 140.
Output:140,126,112,..14
*/

class ReverseTable{

	public static void main(String n[]){
	
		int num = 14;
		System.out.println("Reverse Table of 14 -\n");

		for (int i=10 ; i>=1 ; i--){
		
			System.out.println(num*i);
		}
	}
}
