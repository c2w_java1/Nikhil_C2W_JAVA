/*
5. Write a program to print the odd numbers from 1 - 70.
Output:1,3,5,7,...69
*/

class Odd{

	public static void main (String n[]){
			
			System.out.println("Odd Numbers between 1-70 :- \n");	

			for (int i = 1; i<=70 ; i++){
			
				if(i%2==1){
				
					System.out.println(i);
				}
			}
	}
}
