/*
1. Write a program to print the first 10 numbers.
Output: 1, 2, 3,4...10
*/

class FirstTenNum{

	public static void main(String n[]){
	
		for (int i = 1; i<=10 ; i++){

			System.out.println(i);
		}
	
	}
}
