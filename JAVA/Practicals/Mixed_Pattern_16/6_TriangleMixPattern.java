/*
 * Q6 Write a program to print the factorial of the number.
Rows = 3
c
3 2
c b a

Rows = 4
d
4 3
d c b
4 3 2 1
*/

import java.util.*;

class TriangleMixPattern{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){

			int ch = 96+rows;
                        int num = rows;	

			for(int j=1; j<=i; j++){
			
				if(i%2==0){
					System.out.print(num + "\t");
					num--;
				} else {
				
					System.out.print((char)ch + "\t");
                                        ch--;
				}
			}
			num = rows + i;
			System.out.println("\n");
		}
	}
}
