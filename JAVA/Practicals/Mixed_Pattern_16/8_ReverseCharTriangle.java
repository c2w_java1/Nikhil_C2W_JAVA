/*
Q8 Write a program to reverse the given number.
Rows = 3
F E D
C B
A
Rows = 4
J I H G
F E D
C B
A
*/

import java.util.*;

class ReverseCharTriangle{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter no. of Rows : ");
		int rows = sc.nextInt();

		int num = 0;
		for(int i=rows; i>0; i--){
		
			num +=i;
		}

		int ch = 64+num;
		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=rows-i+1; j++){
			
				System.out.print((char)ch +"\t");
				ch--;
			}
			System.out.println("\n");
		}
	}
}
