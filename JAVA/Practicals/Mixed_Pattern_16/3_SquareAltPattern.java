/*
 Q3 Write a program to print the following pattern
Number of rows = 3
C B A
1 2 3
C B A
Number of rows = 4
D C B A
1 2 3 4
D C B A
1 2 3 4
 */

import java.util.*;

class SquareAltPattern{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){

			int num = 1;	
			int ch = 64+rows;	

			for(int j=1; j<=rows; j++){
			
				if(i%2==0){
					System.out.print(num + "\t");
					num++;
				}else{
				
					System.out.print((char)ch +"\t");
					ch--;
				}
			}
			
			System.out.println("\n");
		}
	}
}
