/*
7. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
4 c 2 a
3 b 1
2 a
1
Rows = 5
5 d 3 b 1
4 c 2 a
3 b 1
2 a
1
*/

import java.io.*;

class TriangleMix7{

	public static void main(String n[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());

		int num = rows;
		int ch = 96+rows;

		for(int i=1; i<=rows; i++){
		
			for(int j=1; j<=rows-i+1; j++){
			
				if(j%2==0){
				
					System.out.print((char)ch +"  ");
				} else {
			
					System.out.print(num +"  ");
				}
				ch--;
				num--;
			}
			ch=96+rows-i;
			num=rows-i;
			System.out.println("\n");

		}

	}
}

