/*
4. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
F E D
C B
A
Rows = 4
J I H G
F E D
C B
A
*/

import java.io.*;

class CharTriangle{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter No. of Rows : ");
		int rows = Integer.parseInt(br.readLine());

		int ch = 64+(rows*rows*rows)-rows;

		for(int i=1;i<=rows; i++){
		
			for(int j=rows; j>=i; j--){
			
				System.out.print((char)ch +"  ");
				ch--;
			}
			System.out.println("\n");
		}

	}
}
