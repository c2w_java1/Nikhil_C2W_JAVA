class FloatDemo{
	 public static void main(String[] args){
	 	int n1 = 959697;
		float f1 = 959697.12345f;
		double d1 = f1;

		System.out.println(n1);
		System.out.println(f1);
		System.out.println(d1);
	 }
}


// output-->

//959697
//959697.1
//959697.125
