class CharDemo{
	public static void main(String [] args){
		char ch1 = "1";
		char ch2 = "0";

		System.out.println(ch1);
		System.out.println(ch2);
	}
}

/*output-->
 
 nikhilbare04@Nikhil-Bare-04:/mnt/e/java/Practicals/Datatypes$ javac code12.java
code12.java:3: error: incompatible types: String cannot be converted to char
                char ch1 = "1";
                           ^
code12.java:4: error: incompatible types: String cannot be converted to char
                char ch2 = "0";
                           ^
2 errors
 */
