class Integer{
	public static void main(String[] args){
		int n1 = 'a';
		float n2 = n1;
		System.out.println(n2); 	//97.02
		
		n2 = 20.005; 		// error 
		System.out.println(n2);
	}
}

/* output-->
 
   nikhilbare04@Nikhil-Bare-04:~$ javac code15.java
code15.java:7: error: incompatible types: possible lossy conversion from double to float
                n2 = 20.005;
                     ^
1 error
 
  */
