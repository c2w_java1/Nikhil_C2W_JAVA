/*
 * 3. Write a program to print the even elements in the array. Take input from the user.
Example :
Enter size : 10
Array:
10 11 12 13 14 15 16 17 18 19
Output :
10
12
14
16
18
*/

import java.io.*;

class EvenArray{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Size of array : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");

		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.print("Element in Array : ");
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]%2==0){

				System.out.print(arr[i]+"\t");
			}
		}
		System.out.println();
               
	}
}
