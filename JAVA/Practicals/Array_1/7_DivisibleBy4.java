/*
 * 7. Write a program to print the elements of the array which is divisible by 4. Take input
from the user.
Example:
Enter size: 10
14 20 18 9 11 51 16 2 8 10
Output:
20 is divisible by 4 and its index is 1
16 is divisible by 4 and its index is 6
8 is divisible by 4 and its index is 8
*/

import java.io.*;

class Divisible{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size for Array : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the Array Elements : ");
		for(int i=0; i<size; i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Elements in Array Divisible by 4  ---");
		for(int i=0; i<size; i++){
		
			if(arr[i]%4==0){
				
				System.out.println(arr[i] +" is Divisible by 4 & it's Index is - "+i);
		
			}
		}
		System.out.println("\n");

	}
}
