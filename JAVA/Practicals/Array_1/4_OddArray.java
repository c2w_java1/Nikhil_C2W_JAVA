/*
 * 4. Write a program to print the sum of odd elements in an array.Take input from the user.
Example:
Enter size: 10
Array:
1 2 3 4 2 5 6 2 8 10
Output :
Sum of odd elements : 9
*/

import java.io.*;

class OddArray{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Size of array : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");

		for(int i=0;i<arr.length;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.print("Element in Array : ");
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]%2==1){

				System.out.print(arr[i]+"\t");
			}
		}
		System.out.println();
               
	}
}
