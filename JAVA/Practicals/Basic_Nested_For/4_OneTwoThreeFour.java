/*
Q3 Write a program to print the following pattern
	Number of rows = 4
1 2 3 4
1 2 3 4
1 2 3 4
1 2 3 4
*/

class UpToFour{

	public static void main(String n[]){
	
		for(int i=1 ; i<=4 ;i++){
		
			for(int j=1 ; j<=4 ;j++){
			
				System.out.print(j +"  ");
			}
			System.out.println();
		}
	}
}
