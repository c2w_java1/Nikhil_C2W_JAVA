/*
 Q10 Write a program to print the following pattern
Number of rows = 3
1 2 3
2 3 4
3 4 5
*/

class Pattern2{

	public static void main(String n[]){
	
		int num = 1;

		for(int i=1; i<4; i++){
		
			for(int j=1; j<4; j++){
			
				System.out.print(num +"   ");
				num++;
			}
			num-=2;
			System.out.println();
		}
	}
}
