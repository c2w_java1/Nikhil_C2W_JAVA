/*
Q5 Write a program to print the following pattern
Number of rows = 3
A C E
G I K
M O Q
*/

class Pattern{

	public static void main(String n[]){
	
	
		char ch = 'A';

		for(int i=1; i<=3; i++){
		
			for(int j=1; j<=3; j++){
			
				System.out.print(ch +"  ");
				ch+=2;
			}
			System.out.println();

		}
	}
}
