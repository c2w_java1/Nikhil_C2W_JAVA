/*
Q4 Write a program to print the following pattern
Number of rows = 4
1 1 1 1
2 2 2 2
3 3 3 3
4 4 4 4
*/

class Four{

	public static void main(String n[]){
	
		int num = 1;
		for(int i=1 ; i<=4 ;i++){
		
			for(int j=1 ; j<=4 ; j++){
			
				System.out.print(num +"  ");
			}
			num++;
			System.out.println();
		}
	}
}
