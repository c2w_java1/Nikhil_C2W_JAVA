/*
Q9 Write a program to print the following pattern
Number of rows = 4
1 2 3 4
4 5 6 7
7 8 9 10
10 11 12 13
*/

class NumPattern{

	public static void main(String n[]){
	
		int num = 1;

		for(int i=1; i<=4 ; i++){
		
			for (int j=1; j<=4; j++){
			
				System.out.print(num +"    ");
				num++;
			}
			num--;
			System.out.println();
		}
	
	}
}
