/*
 Q10 Write a program to print the following pattern
Number of rows = 4
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7
*/

class Pattern1{

	public static void main(String n[]){
	
		int num = 1;

		for(int i=1; i<=4; i++){
		
			for(int j=1; j<=4; j++){
			
				System.out.print(num +"   ");
				num++;
			}
			num-=3;
			System.out.println();
		}
	}
}
