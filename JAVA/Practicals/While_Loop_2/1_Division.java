/*
1. 1.Write a program to print the digits from the given number which is
divisible by 2
Input: 2569185
Output: digits divisible by 2 are: 862

*/

class DivisibleBy2{

	public static void main(String n[]){
	
		int num = 2569185;
	
		System.out.print("Digits Divisible By 2 are - ");

		while(num>0){
		
			int rem = num%10;
			
			if(rem%2==0){
			
				System.out.print(rem +" ");
			}
			num/=10;
		}
		System.out.println();
		
	}
}
