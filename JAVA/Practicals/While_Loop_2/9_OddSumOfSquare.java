/*
1. 9.Write a program to calculate the sum of squares of Odd digits in the given number.
Input: 2469185
Output: 107
*/

class SquareSum{

	public static void main(String n[]){
	
		int num = 2469185;

		System.out.print("Sum of Square Of Odd Digits in Number "+num+" is :");

		int sum = 0;
		while(num>0){
		
			int rem = num%10;

			if(rem%2==1){
			
				sum+=rem*rem;
			
			}
			num/=10;
		}
		System.out.print(sum+"\n");
	}
}
