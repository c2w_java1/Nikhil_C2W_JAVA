/*
1. 7.Write a program to print the sum of even digits
Input: 256985
Output:
Sum of even digits: 16
*/

class Sum{

	public static void main(String n[]){
	
		int num = 256985;

		System.out.print("Sum Of Even Digits in Number "+num+" is :");

		int sum = 0;
		while(num>0){
		
			int rem = num%10;

			if(rem%2==0){
			
				sum+=rem;
			
			}
			num/=10;
		}
		System.out.print(sum+"\n");
	}
}
