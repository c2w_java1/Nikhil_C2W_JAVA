/*
 1. 4.Write a program to print the square of odd digits of a given number.
Input: 256985
Output: 25 81 25
*/

class Square{

	public static void main (String n[]){
	
		int num = 256985;

		System.out.print("Square of Odd Digits in " +num +" are : " );
		while(num>0){
		
			int rem = num%10;

			if (rem%2==1){
			
				System.out.print(rem*rem +"  ");
			}

			num/=10;
		}
		System.out.println();
	}
}
