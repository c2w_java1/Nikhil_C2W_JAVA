/*
1. 2.Write a program to print the digits from the given number which is not divisible by 3.
Input: 2569185
Output: digits not divisible by 3 are: 58152
*/

class NotDivisibleBy3{

	public static void main(String n[]){
	
		int num = 2569185;
		
		System.out.print("Digits in Given Number not Divisible by 3 are - ");

		while(num>0){
		
			int rem = num%10;

			if(rem%3==0){
		

			} else {
		
				System.out.print(rem +" ");
			}
	
			num/=10;

		}

		System.out.println();
	}
}
