/*
 * 1. WAP to check whether the given number is a Perfect number or not.
Input 1:
Enter a number: 6
Output 1 :
6 is a Perfect Number.

Input 2:
Enter a number: 15
Output 2 :
15 is not a Perfect Number.
(Perfect Number:
A perfect number is a positive integer that is equal to the sum of its proper divisors, excluding itself. The
simplest way to understand this is by looking at the smallest perfect number, which is 6. The divisors of 6
(excluding itself) are 1, 2, and 3, and if you add these divisors together (1 + 2 + 3), the sum is 6.)
*/

import java.util.*;

class PerfectNumber{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();
		
		int sum = 0;
		for(int i=1; i<=num/2; i++){
		
			if(num%i==0){
				sum += i;
			}
		}
		if(sum==num){
		
			System.out.println(num+" is a Perfect Number.");
		} else {
			System.out.println(num+" is NOT Perfect Number.");
		}
	}
}
