/*
 * 3. WAP to check whether the given number is a Deficient number or not.
Input 1:
Enter a number: 25
Output 1 :
25 is not a Deficient Number.
Input 2:
Enter a number: 12
Output 2 :
12 is a Deficient Number.
Deficient number
A deficient number is a positive integer that is greater than the sum of its proper divisors, excluding itself.
In other words, the sum of the proper divisors of a deficient number is less than the number itself.
et's take the number 8:
The proper divisors of 8 are 1, 2, and 4.
The sum of these proper divisors is 1 + 2 + 4 = 7.
As 7 is less than 8 it is a deficient number.
*/

import java.util.*;

class DeficientNum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		int sum = 0;
		
		for(int i=1; i<=num/2; i++){
			
			if(num%i==0){
				sum += i;
			}
		}
		if(sum < num){
			System.out.println(num +" is Deficient Number.");
		} else{
			System.out.println(num +" is NOT Deficient Number.");
		}
		
		System.out.println(sum);
	}
} 
