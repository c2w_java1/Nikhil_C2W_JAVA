/*
 * 5. WAP to check whether the given number is Automorphic or not.
Input 1:
Enter a number: 25
Output 1 :
25 is an Automorphic Number.
Input 2:
Enter a number: 4
Output 2 :
4 is not an Automorphic Number.
Automorphic Number
A number is automorphic if its square ends with the number itself.
The square of 25 is 625.
Since 25 appears at the end of 625, 25 is an automorphic number.
*/

import java.util.*;

class Automorphic{

	public static void main(String n[]){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		int temp = num;
		int countND= 0;
		//to count the digits in the number given
		while(temp>0){
			countND++;
			temp /=10;
		}

		int square = num*num;

		//to find out the last n digits in the square
		int rem = 0;
		int rev = 0;
		temp = square;
		for(int i=countND; i>0; i--){
			rem = temp%10;
			rev = rev*10+rem;
			temp /=10;
				
		}
		
		int last = 0;
		while(rev>0){
			rem = rev%10;
			last = last * 10 + rem;

			rev /=10;
		}


		if(num == last){
			System.out.println(num +" is Automorphic Number.(Square = "+square+")");
		} else {
			System.out.println(num +" is NOT Automorphic Number.(Square = "+square+")");
		}
	}
}
