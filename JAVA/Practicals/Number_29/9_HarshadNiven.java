/*
 * 9. WAP to check whether the given number is Harshad/ Niven number or not.
Input 1:
Enter a number: 10
Output 1 :
10 is a Harshad Number.
Input 2:
Enter a number: 11
Output 2 :
11 is not a Harshad Number.
Harshad number
A number is a Harshad number if it is divisible by the sum of its own digits.
let's consider the number 18:
The sum of the digits of 18 is 1 + 8 = 9.
Since 18 is divisible by 9 i.e(18 % 9 = 0), it is a Harshad number.
*/

import java.io.*;
class HarshadNum{

	public static void main(String n[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Number : ");
		int num = Integer.parseInt(br.readLine());

		int temp = num;
		int sum = 0;
		while(temp>0){
			int rem = temp%10;
			sum += rem;
			temp /= 10;
		}

		if(num%sum==0){
			System.out.println(num +" is a Harshad Number.");
		} else {
			System.out.println(num +" is NOT Harshad Number.");
		}
	}
}
