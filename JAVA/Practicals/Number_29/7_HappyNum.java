/*
 * 7. WAP to check whether the given number is a Happy number or not.
Input 1:
Enter a number: 10
Output 1 :
10 is a Happy Number.
Input 2:
Enter a number: 12
Output 2 :
12 is not a Happy Number.
Happy number
A happy number is a positive integer that, when iteratively replaced by the sum of the squares of its digits,
eventually reaches 1.
Let’s Consider a number 10:
1^2 + 0^2 = 1
It’s a happy number.
*/

import java.util.*;

class Happy{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();
		
		int temp = num;
		//1.Separation of digits in numbers.
		//2.find Sum of Square of all Digits
		//if sum!=1 then replce num with sum 

		int sum = 0;
		int sumP = temp;
		while(temp>0){
			
			if(sum == 1 || sum == 4){
                                break;
			} else {
				
				while(temp>0){
					int rem = temp%10;
					sum += rem*rem;
					temp /=210;
				}
				temp=sum;
			}
		}


		if(sum == 1){
			System.out.println(num +" is a HAPPY NUMBER.(Sum = "+sum+")");
		} else {
			System.out.println(num +" is a NOT Happy Number.(Sum = "+sum+")");
		}
	}
}
