/*
 * 2. WAP to check whether the given number is a Strong number or not.
Input 1:
Enter a number: 6
Output 1 :
6 is not a Strong Number.
Input 2:
Enter a number: 145
Output 2 :
145 is a Strong Number.
Strong Number:
A Strong number is a number such that the sum of the factorials of its digits is equal to the number itself.
Take the number 145:
The factorial of 1 is
1!=1,

The factorial of 4 is
4!=4×3×2×1=24,
The factorial of 5 is
5!=5×4×3×2×1=120
Adding these factorials together,
1+24+120=145, which means 145 is a Strong number.
*/

import java.util.*;

class StrongNum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		int temp = num;

		//for Seperating each Digits in Number
		int sum = 0;
		while(temp>0){

			int rem = temp%10;

			int factorial = 1;
			while(rem>0){
				factorial *= rem;
				rem--;
			}
			sum += factorial;
			temp /=10;
		}
		if(sum==num){
			System.out.println(num+" is Strong Number.");
		} else {
		
			System.out.println(num+" is NOT Strong Number.");
		}
	}
}
