/*
 * 8. WAP to check Whether a number is a Duck Number or not.
Input 1:
Enter a number: 101150
Output 1 :
101150 is a Duck Number.
Input 2:

Enter a number: 82569
Output 2 :
82569 is not a Duck Number.
Duck Number
A Duck number is any number that contains the digit zero but doesn't start with it.
102 is a Duck number because it contains the digit zero and doesn't start with it.
*/

import java.util.*;

class Duck{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		int temp = num;
		int count = 0;
		int flag = 0;
		while(temp>0){
		
			count++;
			int rem = temp%10;
			if(rem == 0 && count>0){
				flag = 1;
				break;
			}
			temp /= 10; 
		}
		if(flag == 1){
			System.out.println(num +" is a DUCK Number.");
		} else{
			System.out.println(num +" is NOT DUCK Number.");
		}
	}
}
