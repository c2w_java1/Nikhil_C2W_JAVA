/*
 * 10. WAP to check whether the given number is an armstrong number or not.
Input 1:
Enter a number: 153
Output 1 :
153 is an Armstrong Number.
Input 2:
Enter a number: 120
Output 2 :

120 is not an Armstrong Number.
Armstrong number
it's a number where the sum of its own digits, each raised to the power of the number of digits, equals the
number itself.
For example, let's take the number 153:
The number of digits in 153 is 3.
1^3+5^3+3^3=1+125+27=153
153 is an armstrong number.
*/

import java.util.*;

class Armstrong {

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		int temp = num;
		int count = 0;
		while(temp>0){
			count++;
			temp /= 10;
		}

		temp = num;
		int sum = 0;
		while(temp>0){
			int rem = temp%10;
			int power = 1;
			for(int i=1; i<=count; i++){
				power *= rem;
			}
			sum += power;
			temp /= 10;
		}

		if(num == sum){
			System.out.println(num +" is ARMSTRONG Number.");
		} else {
			System.out.println(num +" is NOT ARMSTRONG Number.");
		}
	}
}
