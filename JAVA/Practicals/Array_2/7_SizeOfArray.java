/*
 * 7.WAP to print the array , if the user given size of an array is even then print the alternate
elements in an array, else print the whole array.
Example 1:
Input:
Enter the size
5
Enter elements:
1
2
3
4

5
Output:
Array elements are:
1
2
3
4
5
*/

import java.util.*;

class Size{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Arary Elements are ::  ");
		for(int i=0; i<arr.length; i++){
			
			if(size%2==0){
			
				System.out.print(arr[i] +"  ");
				i++;
			} else{
			
				System.out.print(arr[i] +"  ");
			}
		}
	}
}  
