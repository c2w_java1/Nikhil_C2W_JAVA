/*
 * 10. WAP to print the Maximum element in the array.
Example:
Input:
Enter the size
5
Enter elements:
7
81
65
12
23
Output:
Maximum number in the array is found at pos 1 is 81
*/

import java.util.*;

class Maximum{
	
	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}	

		int max = arr[0];
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]>max){
			
				max = arr[i];
			}
		}

		System.out.println("Maximum Element in Array is -> "+max);
	}
}

