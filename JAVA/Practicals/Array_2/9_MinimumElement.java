/*
 * 9. WAP to print the minimum element in the array, where you have to take the size and
elements from the user.
Example:
Input:
Enter the size
5
Enter elements:
5
6
9
-9
17
Output:

Minimum number in the array is : -9
*/

import java.util.*;

class Minimum{
	
	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}	

		int min = arr[0];
		for(int i=0; i<arr.length; i++){
		
			if(arr[i]<min){
			
				min = arr[i];
			}
		}

		System.out.println("Minimum Element in Array is -> "+min);
	}
}

