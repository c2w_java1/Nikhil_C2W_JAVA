/*
 * 3.WAP to check if there is any vowel in the array of characters if present then print its
index, where you have to take the size and elements from the user.
Example:
Input:
Enter the size
5
Enter elements:
arEKO
Output:
vowel a found at index 0
vowel E found at index 2
vowel O found at index 4
*/

import java.util.*;

class Vowels{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		char ch[]= new char[size];

		System.out.println("Enter Array Elements in Character : ");
		for(int i=0; i<ch.length; i++){
			ch[i] = sc.next().charAt(0);
		}

		
		for(int i=0; i<ch.length; i++){
		
			if(ch[i]=='a' || ch[i]=='A' || ch[i]=='e' || ch[i]=='E' || ch[i]=='i' || ch[i]=='I' || ch[i]=='o' || ch[i]=='O' || ch[i]=='u' || ch[i]=='U'){
			
				System.out.println("Vowel "+ch[i] +" is Found at Index : "+i);
			}
		}
	}
}
