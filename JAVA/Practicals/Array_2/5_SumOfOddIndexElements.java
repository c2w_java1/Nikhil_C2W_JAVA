/*
 * 5.Write a program to print the sum of odd indexed elements, in an array. Where you have
to take size input and elements input from the user .
Example:
Input:
Enter the size
5
Enter elements:
4
8
2
6
7
Output:
Sum of odd indexed elements : 14
*/

import java.util.*;

class OddIndexSum{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Elements in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		int sum = 0;
		for(int i=0; i<arr.length; i++){
		
			if(i%2==1){
			
				sum +=arr[i];
			}
		}
		System.out.println("\nSum of Odd Index Elements -> "+sum);
	}
}
