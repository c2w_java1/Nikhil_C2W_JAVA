/*
 * 4.WAP to search a specific element in an array and return its index. Ask the user to
provide the number to search, also take size and elements input from the user.
Example:
Input:
Enter the size
5
Enter elements:
12
144
13
156
8
Enter the number to search in array:
8

Output:
8 found at index 4
*/

import java.util.*;

class Search{

	public static void main(String n[]){
	
		Scanner sc = new Scanner (System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Element in Array : ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		for(int i=0; i<arr.length; i++){
		
			if(arr[i] == num){
			
				System.out.println(num +" is Fount at Index "+i);
			}
		}
	}
}





