/*
4. A student has passed his 12 th standard exam he thinks he might get marks upto 85
percent. Suggest the better career field depending upon the marks assumption.(take
hardcoded values)
Input 1: percentage >85.00;
Output: Medical
Input 2: percentage <=85.00 and percentage >75.00;
Output: Engineering;
Input 1: percentage<=75.00 and percentage>=65.00;
Output: pharmacy or bachelor in science;
You have to write the code using different values of percentage.
*/

class Student{

	public static void main(String n[]){
	
		float percentage = 66.50f;

		if (percentage > 85){
		
			System.out.println("Take Admission for Engineering.");

		} else if(percentage <= 85 && percentage >= 75 ) {

                        System.out.println("Take Admission For Medical Cource.");
              
	      	} else if(percentage <= 75 && percentage >= 60 ) {

                        System.out.println("Take Admission For BCA.");
                
		} else if(percentage <= 60 && percentage >= 50 ) {

                        System.out.println("Take Admission For BBA.");
                } else {
		
			System.out.println("Take Admission For ITI.");
		}
	}
}
