/*
6. Write a program to verify the age of the voter where the age should be positive. (that
means no negative values are allowed, take hardcoded values).
Input 1: 21
Output: Valid age for voting
Input 2: -31
Output: Invalid age
*/

class Voter{

	public static void main(String n[]){
	
		int age = -2;

		if (age > 0){
			if (age >= 18){
		
				System.out.println("Valid age For Voting.");
			} else {

	                        System.out.println(" You are Eligible For Voting After  Age 18.");
        	        }
		} else {
		
			System.out.println("Invalid Age..!");
		}
        }
}
 
