/*
7. Calculate the profit or loss.
Write a program that takes cost price and selling price(take it hardcoded) and calculates its
profit or loss.
Input 1:
sellingPrice: 1200
costPrice: 1000
Output: Profit of 200
Input 2:
sellingPrice: 300
costPrice: 500
Output: loss of 200
Input 3:
sellingPrice: 900
costPrice: 900
Output: No profit no loss
*/

class ProfitLoss {

	public static void main(String n[]){
	
		int sellingPrice = 1400;
		int costPrice = 1000;

		if(sellingPrice > costPrice){
		
			System.out.println("Profit of : "+(sellingPrice - costPrice));
	
		} else if(sellingPrice == costPrice){
		
			 System.out.println("No Profit, No Loss");

		} else {
			
			 System.out.println("Loss of : "+(sellingPrice - costPrice));
		}
	}
}
