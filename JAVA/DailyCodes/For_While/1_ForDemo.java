class ForDemo{

	public static void main(String n[]){
	
		System.out.println("Without For ");

		for(int i = 1 ; i<=4 ; i++){
		
			System.out.println("In For Loop");
		}
	}
}

/*
 Without For
In For Loop
In For Loop
In For Loop
In For Loop
 */
