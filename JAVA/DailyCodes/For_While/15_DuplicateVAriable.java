class VariableScope{

	public static void main(String n[]){
	
		int i=1;
		for(; i<=10 ;){
		
			System.out.println("In For Loop ");
			i++;
		}

		int i = 1;		//Same variable but in Diffrent Scope
		while(i<=10){
			System.out.println("\nIn while Loop ");
			i++;
		}
	}
}
/*
 * nikhilbare04@Nikhil-Bare-04:/mnt/e/java/DailyCodes/For_While$ javac 15_DuplicateVAriable.java
15_DuplicateVAriable.java:12: error: variable i is already defined in method main(String[])
                int i = 1;              //Same variable but in Diffrent Scope
                    ^
1 error
*/
