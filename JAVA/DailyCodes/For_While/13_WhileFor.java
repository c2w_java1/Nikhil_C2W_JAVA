class WhileFor{

	public static void main(String n[]){
	
		for(int i = 1; i<=10 ; i++){
		
			System.out.println("In For Loop ");
		}

		int i = 1;		//Same variable but in Diffrent Scope
		while(i<=10){
			System.out.println("\nIn while Loop ");
			i++;
		}
	}
}
