class LogicalOper4{

	public static void main(String n[]){
		
		boolean x = 10;
		boolean y = 22;
		
		System.out.println((x<y)&&(y>x));
		System.out.println(x);
		System.out.println(y);
	}
}

/*
 17_LogicalOper4.java:5: error: incompatible types: int cannot be converted to boolean
                boolean x = 10;
                            ^
 */
