class LeftShiftOper{

	public static void main(String n[]){
	
		int x = 5;

		System.out.println(x<<2);
	}
}

/*
 We are shifting 2 bits to left that means binary 
 of 5 is 0000 0101 Which will become 00010100, this is nothing but 20.

 */
