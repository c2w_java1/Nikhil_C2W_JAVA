// Datatype Char

class SwitchDemo10{

	public static void main(String N[]){
	
		char ch = 'A';

		System.out.println("Before Switch ...");

		switch(ch){
			
			case 'A' :
				System.out.println("A"); 	//one
				break;

			case 'B' :
				System.out.println("B");
				break;

			case 'C': 
				System.out.println("C");
				break;	

			default :
				System.out.println("In Default State");
		}

		System.out.println("After Switch ...");
	}
}

/*

17_Switch.java:7: error: incompatible types: possible lossy conversion from double to float
                float f = 1.5;
                          ^
17_Switch.java:11: error: selector type float is not allowed
                switch(f){
                      ^
17_Switch.java:13: error: incompatible types: possible lossy conversion from double to float
                        case 1.5 :
                             ^
17_Switch.java:17: error: incompatible types: possible lossy conversion from double to float
                        case 2.0 :
                             ^
17_Switch.java:21: error: incompatible types: possible lossy conversion from double to float
                        case 2.5:
                             ^
5 errors
*/
