
class SwitchDemo1{

	public static void main(String N[]){
	
		int n = 3;

		System.out.println("Before Switch ...");

		switch(n){
			
			case 1 :
				System.out.println("One");

			case 2 :
				System.out.println("Two");

			case 3: 
				System.out.println("Three"); 	//Three
		}

		System.out.println("After Switch ...");
	}
}
