

class SwitchDuplicateCaseLable{

	public static void main(String N[]){
	
		int n = 12;

		System.out.println("Before Switch ...");

		switch(n){
			
			case 11 :
                                System.out.println("11");
                                break;

			case 11 :
				System.out.println("121"); 	
				break;

			case 12 :
				System.out.println("144");
				break;

			case 13: 
				System.out.println("169");
				break;	

			default :
				System.out.println("In Default State");
		}

		System.out.println("After Switch ...");
	}
}

/*
22_Switch_Duplicate_CaseLable.java:17: error: duplicate case label
                        case 11 :
                        ^
1 error
*/
