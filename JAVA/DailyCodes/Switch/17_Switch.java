// Datatype Float - Incompatible type

class SwitchDemo8{

	public static void main(String N[]){
	
		float f = 1.5;

		System.out.println("Before Switch ...");

		switch(f){
			
			case 1.5 :
				System.out.println("One");
				break;

			case 2.0 :
				System.out.println("Two");
				break;

			case 2.5: 
				System.out.println("Three");
				break;	

			default :
				System.out.println("In Default State");
		}

		System.out.println("After Switch ...");
	}
}

/*

17_Switch.java:7: error: incompatible types: possible lossy conversion from double to float
                float f = 1.5;
                          ^
17_Switch.java:11: error: selector type float is not allowed
                switch(f){
                      ^
17_Switch.java:13: error: incompatible types: possible lossy conversion from double to float
                        case 1.5 :
                             ^
17_Switch.java:17: error: incompatible types: possible lossy conversion from double to float
                        case 2.0 :
                             ^
17_Switch.java:21: error: incompatible types: possible lossy conversion from double to float
                        case 2.5:
                             ^
5 errors
*/
