// Datatype Char

class SwitchDemo11{

	public static void main(String N[]){
	
		char ch = 'B';

		System.out.println("Before Switch ...");

		switch(ch){
			
			case 'A' :
				System.out.println("A"); 	
				break;

			case 65 :
                                System.out.println("65");
                                break;

			case 'B' :
				System.out.println("B");
				break;

			case 66 : 
				System.out.println("66");
				break;	

			default :
				System.out.println("In Default State");
		}

		System.out.println("After Switch ...");
	}
}

/*

20_Switch.java:17: error: duplicate case label
                        case 65 :
                        ^
20_Switch.java:25: error: duplicate case label
                        case 66 :
                        ^
2 errors

*/
