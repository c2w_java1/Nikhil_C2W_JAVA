
class SwitchDemo9{

	public static void main(String N[]){
	
		float n = 1.5f;

		System.out.println("Before Switch ...");

		switch(n){
			
			case 1 :
				System.out.println("One");
				break;

			case 2 :
				System.out.println("Two");
				break;

			case 3: 
				System.out.println("Three");
				break;	

			default :
				System.out.println("In Default State");
		}

		System.out.println("After Switch ...");
	}
}

/*
 18_Switch.java:10: error: selector type float is not allowed
                switch(n){
                      ^
1 error
 
 */
