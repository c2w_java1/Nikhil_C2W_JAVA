// Datatype String

class SwitchDemo12{

	public static void main(String N[]){
	
		String s = "Nikhil";

		System.out.println("Before Switch ...");

		switch(s){
			
			case "Nikhil" :
				System.out.println("Bare"); 	
				break;

			case "Avish" :
				System.out.println("Pacharne");
				break;

			case "Harshal" : 
				System.out.println("Dongare");
				break;	

			default :
				System.out.println("In Default State");
		}

		System.out.println("After Switch ...");
	}
}

