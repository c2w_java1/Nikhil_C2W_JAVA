
class SwitchDemo5{

	public static void main(String N[]){
	
		int n = 2;

		System.out.println("Before Switch ...");

		switch(n){
			
			case 1 :
				System.out.println("One");
				break;

			case 2 :
				System.out.println("Two");
				break;

			case 3: 
				System.out.println("Three");
				break;	

			default :
				System.out.println("In Default State");
		}

		System.out.println("After Switch ...");
	}
}
