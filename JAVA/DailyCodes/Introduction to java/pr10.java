class Core2web10{
	public static void main(){
		System.out.println("Without String [] args");
	}
}
/*output-->
 Error: Main method not found in class Core2web10, please define the main method as:
   public static void main(String[] args)
or a JavaFX application class must extend javafx.application.Application
*/
