class cw{
	static void main(String n[]){
		
		System.out.println("Start Main");
		System.out.println("End Main");
	}
}

/*
 Error: Main method not found in class cw, please define the main method as:
   public static void main(String[] args)
or a JavaFX application class must extend javafx.application.Application
 */
