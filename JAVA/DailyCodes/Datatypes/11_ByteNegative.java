//Range of byte -128 to 127

class ByteNegativeValues{
	
	public static void main(String n[]){
		
		byte age = -130;

		System.out.println("Age - " +age);
	}
}

/*output-->
11_ByteNegative.java:7: error: incompatible types: possible lossy conversion from int to byte
                byte age = -130;
                           ^
1 error
*/
