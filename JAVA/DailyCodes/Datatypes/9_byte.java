//Range of byte -128 to 127

class Byte{
	
	public static void main(String n[]){
		
		byte age = 150;

		System.out.println("Age - " +age);
	}
}

/*output-->
9_byte.java:7: error: incompatible types: possible lossy conversion from int to byte
                byte age = 150;
                           ^
1 error
*/
