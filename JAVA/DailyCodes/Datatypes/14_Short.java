//Range of Short -32768 to 32767

class ShortError{
	
	public static void main(String m[]){

		short s = 32768;

		System.out.println(s);		//error
	
	}
}

/*o/p-->
14_Short.java:7: error: incompatible types: possible lossy conversion from int to short
                short s = 32768;
                          ^
1 error
*/
