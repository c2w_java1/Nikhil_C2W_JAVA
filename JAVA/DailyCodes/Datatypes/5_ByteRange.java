//Range of byte -128 to 127

class ByteRange{
	
	public static void main(String n[]){
		
		byte age = 128;

		System.out.println("Age - " +age);
	}
}

/*Output-->error:
5_ByteRange.java:7: error: incompatible types: possible lossy conversion from int to byte
                byte age = 128;
                           ^
1 error
*/
