class ArrayException{

	public static void main(String n[]){
	
		int num[]={10,25,44,30}; 		//size-3
	
		System.out.println("Num in Array at 0 index : " +num[0]);	//1
		System.out.println("Num in Array at 1 index : " +num[1]);	//2
		System.out.println("Num in Array at 2 index : " +num[2]);	//3

		System.out.println("Num in Array at 5 index : " +num[5]);	//Exception ArrayIndexOut of Bound

	}
}
/*
nikhilbare04@Nikhil-Bare-04:/mnt/e/java/DailyCodes/Array/1D_Array$ java ArrayException
Num in Array at 0 index : 10
Num in Array at 1 index : 25
Num in Array at 2 index : 44
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 5 out of bounds for length 4
        at ArrayException.main(18_ArrayException.java:11)
	*/
