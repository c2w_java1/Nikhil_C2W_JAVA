class ArrayInitialization{

	public static void main(String n[]){
	
		int num[]={1,2,3}; 		//size-3
	
		System.out.println("Num in Array at 0 index : " +num[0]);	//1
		System.out.println("Num in Array at 1 index : " +num[1]);	//2
		System.out.println("Num in Array at 2 index : " +num[2]);	//3

	}
}
