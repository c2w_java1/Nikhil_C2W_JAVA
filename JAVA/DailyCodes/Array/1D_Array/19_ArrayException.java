class ArrayException19{

	public static void main(String n[]){
	
		int num[]={10,25,44,30}; 		//size-3
	
		System.out.println("Num in Array at 0 index : " +num[10]);	//1
		System.out.println("Num in Array at 1 index : " +num[11]);	//2
		System.out.println("Num in Array at 2 index : " +num[12]);	//3

		System.out.println("Num in Array at 5 index : " +num[5]);	//Exception ArrayIndexOut of Bound

	}
}
/*
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 10 out of bounds for length 4
        at ArrayException19.main(19_ArrayException.java:7)

	*/
