

class DefaultVal{

	public static void main(String n[]){
	
		int arr[] = new int[5];

		System.out.println("element at 0 index : " +arr[0]);
		System.out.println("element at 1 index : " +arr[1]);
		System.out.println("element at 2 index : " +arr[2]);
		System.out.println("element at 3 index : " +arr[3]);
		System.out.println("element at 4 index : " +arr[4]);

		arr[0]=11;
		arr[1]=22;
		arr[2]=33;
		arr[4]=55;

		System.out.println("element at 0 index : " +arr[0]);
		System.out.println("element at 1 index : " +arr[1]);
		System.out.println("element at 2 index : " +arr[2]);
		System.out.println("element at 3 index : " +arr[3]);
		System.out.println("element at 4 index : " +arr[4]);


	}
}
/*
nikhilbare04@Nikhil-Bare-04:/mnt/e/java/DailyCodes/Array/1D_Array$ java DefaultVal
element at 0 index : 0
element at 1 index : 0
element at 2 index : 0
element at 3 index : 0
element at 4 index : 0
element at 0 index : 11
element at 1 index : 22
element at 2 index : 33
element at 3 index : 0
element at 4 index : 55
*/
