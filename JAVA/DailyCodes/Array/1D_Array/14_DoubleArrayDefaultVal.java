//import java.util.*;


class DoubleDefaultValues1{

	public static void main(String n[]){
	
		double []empId = new double[3];
		
                System.out.println("EmpId 1 : " +empId[0]);
                System.out.println("EmpId 2 : " +empId[1]);
                System.out.println("EmpId 3 : " +empId[2]);

		/*
		empId[0] = 10;
		empId[1] = 20;
		empId[2] = 30;

		System.out.println("EmpId 1 : " +empId[0]);
		System.out.println("EmpId 2 : " +empId[1]);
		System.out.println("EmpId 3 : " +empId[2]);
		*/
	}
}
/*
EmpId 1 : 0.0
EmpId 2 : 0.0
EmpId 3 : 0.0
*/

