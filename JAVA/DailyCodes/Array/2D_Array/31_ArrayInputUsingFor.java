import java.util.*;

class ArrayInput31{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Size is - " +arr.length);

		for(int i=0; i<arr.length; i++){
		
			System.out.print("Enter Element : ");
			arr[i]= sc.nextInt();
		}

		System.out.print("Elements in Array => ");

		for(int i=0; i<arr.length; i++){
		
			System.out.print(arr[i] +"   ");
		}
	}
}
