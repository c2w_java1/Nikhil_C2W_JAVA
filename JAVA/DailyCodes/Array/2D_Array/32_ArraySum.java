import java.util.*;

class ArraySum32{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Size is - " +arr.length);

		for(int i=0; i<arr.length; i++){
		
			System.out.print("Enter Element : ");
			arr[i]= sc.nextInt();
		}

		int sum = 0;
		System.out.print("Elements in Array => ");

		for(int i=0; i<arr.length; i++){
		
			sum+= arr[i];
			System.out.print(arr[i] +"   ");
		}
		System.out.println("\nSum Of Elements in Array = " +sum);
	}
}
