// +,-,*,/,%

class ArithmeticOperations{
	public static void main(String n []){
		
		int Num1 = 20;
		int Num2 = 10;

		System.out.println("Number 1 --> "+Num1);
		System.out.println("Number 2 --> "+Num2);
		System.out.println("----------------------------------------");
		System.out.println("Addition - " +(Num1 + Num2));
		System.out.println("Subtraction - " +(Num1 - Num2));
		System.out.println("Multiplication - " +(Num1 * Num2));
		System.out.println("Division (Q) - " +(Num1 / Num2));
		System.out.println("Reminder - " +(Num1 % Num2));
	}
}
