/*
Q2 Write a program to take the age of the voter from the user where the age should be positive and check whether he or she is eligible for voting. (that means no negative values are allowed).
Input 1: age = 15
Output: Voter is not eligible for voting.
Input 2: age = 18
Output: Voter is eligible for voting.
Input 2: age = - 16
Output: Invalid age.
*/

import java.util.*;

class Voter{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Voter Age : ");
		int age = sc.nextInt();

		if(age>=18){
		
			System.out.println("Voter is Eligible for voting.");
		} else if(age<18 && age >0) {
		
			System.out.println("Voter is Not-Eligible for voting.");
		} else {
		
			System.out.println("Invalid Age Input");	
		}
	}
}
