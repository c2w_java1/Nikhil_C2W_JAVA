/*
Q9 Write a program to take the range from the user and print the ever numbers.
Input 1: num1 = 5
num * 2 = 16
Output: 6, 8, 10, 12, 14, 16,
Input 2: num1 = -2
num * 2 = 5
Output: - 2, 0, 2, 4
theuer end
*/

import java.util.*;

class Even{

	public static void main(String n[]){
		
		Scanner sc = new Scanner (System.in);

		System.out.print("Enter 1st Number : ");
		int num1 = sc.nextInt();

		System.out.print("Enter 2nd Number : ");
		int num2 = sc.nextInt();
		
		System.out.print("\nEven Num between " +num1 +" & " +num2 +" is :- ");
		for(int i = num1; i<=num2; i++){
		
			if(i%2==0){
			
				System.out.print(i +",  ");
			}
		}
		System.out.println();

	}

}
