/*
Q3 Write a program to take a number as input from the user and check whether the number is divisible by 8 or not.
Input 1: num = 24
Output: 24 is divisible by 8.
Input 2: num = 45
Output: 45 is not divisible by 8.
*/

import java.util.*;

class Division8{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		if(num%8==0){
		
			System.out.println(num +" is Divisible By 8.");
		} else {
		
			System.out.println(num +" is Not-Divisible By 8.");
		}

	}
}
