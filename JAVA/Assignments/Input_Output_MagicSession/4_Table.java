/*
Q4 Write a program to take a number as input from the user and print its table.

Input : num = 9;
Output: 9, 18, 27, 36, 45, 54, 63, 72, 81, 90,
*/

import java.util.*;

class Table{

	public static void main (String n[]){
	
		Scanner sc = new Scanner (System.in);

		System.out.print("Enter Number to Print Table : ");
		int num = sc.nextInt();
		
		System.out.println("\n Table of - " +num +"\n");
		for(int i=1; i<=10 ; i++){
		
			System.out.println(num*i);
		}
	}
}
