/*
Q1 Write a program to take a number as input from the user and check whether the number is even or odd.
Input 1: num = 26
Output: 26 is an even number.
Input 2: num =45:
Output: 26 is an even number.
*/

import java.util.*;

class EvenOdd{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		if(num%2==0){
		
			System.out.println(num +" is an Even Number.");
		} else {
		
			System.out.println(num +" is an Odd Number.");
		}
	}
}
