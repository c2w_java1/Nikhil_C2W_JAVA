/*
Q10 Write a program to take marks of four subjects from the user an print the total obtained marks out of total. (100 marks each subject)

Input 1: math= 95
english= 96
science= 99
marathi= 97
Output: 387 out of 400
*/

import java.util.*;

class Marks{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Marks For Each Subjects -- ");

		System.out.print("Math : ");
		int math = sc.nextInt();

		System.out.print("English : ");
		int english = sc.nextInt();

		System.out.print("Marathi : ");
		int marathi = sc.nextInt();

		System.out.print("Science : ");
		int science = sc.nextInt();

		if(math>100 || english>100 || marathi>100 || science>100){
		
			System.out.println("Please Enter Valid Marks (Out of 100)");
		} else {

			int sum = math + marathi + english + science ;

			System.out.println(sum+" out of 400 ");
		}
	}	
}
