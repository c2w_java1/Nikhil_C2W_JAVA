/*
Q2.
Write a program to print Remarks according to their respective grades.
Input : O
Output : Outstanding
Input : A
Output : Excellent
*/

class GradesS{

	public static void main (String n[]){
	
		char ch = 'C';

		switch(ch){
		
			case 'O':
				System.out.println("Outstanding.");
				break;
			
			case 'A':
				System.out.println("Excellent.");
                                break;

			case 'B':
				System.out.println("Very Good.");
                                break;

			case 'C':
				System.out.println("Good.");
                                break;

			case 'P':
				System.out.println("Pass.");
                                break;

			default :
				System.out.println("Fail.");

		}
	}
}
