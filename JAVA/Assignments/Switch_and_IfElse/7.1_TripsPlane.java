/*if else
Q7.
Write a program for the below example.
If you are a trip planner and you are planning a trip according to the budget of
client
1. For budget 15000 destination is Jammu and Kashmir
2. For budget 10000 destination is Manali
3. For budget 6000 destination is Amritsar
4. For budget 2000 destination is Mahabaleshwar
5. For Other budgets try next time
*/

class Trip {

	public static void main(String n[]){
	
		int budget = 5600;

		if(budget >= 15000 ){
		
			System.out.println("Jammu and Kashmir for budget - " +budget);
		} else if(budget >= 10000 && budget < 10000){

                        System.out.println("Manali for budget - " +budget);
                } else if(budget < 10000 && budget >= 6000 ){

                        System.out.println("Amritsur for budget - " +budget);
                } else if(budget < 6000 && budget >= 2000 ){

                        System.out.println("Mahabaleshwar for budget - " +budget);
                } else {
		
			System.out.println("Try Next Time....!");
		}
	}
}
