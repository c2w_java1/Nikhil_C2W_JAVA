/*
Q8.
Write a program in which takes two numbers if both the numbers are positive
multiply them and provide to switch case to verify whether the number is even or
odd, if the user enters any negative number program should terminate by saying
“Sorry negative numbers not allowed”
*/

class Number {

	public static void main(String n[]){
	
		int num1 = 12;
		int num2 = -4;

		int result = 1;

		if (num1>=0 && num2>=0){
		
			result = num1*num2;
			System.out.println("Multiplication -> " +num1 +" X " +num2 +" = " +result);	
		} else {
		
			System.out.println("Sorry negative numbers not allowed.");
		} 
	}
}
