/*
 Q1.Switch
Write a program to check whether the given number is odd or even.
Input : 45
Output : 45 is an odd number.
 */

class OddEvenS{

	public static void main(String n[]){
	
		int num = 45;
		int ans = num % 2;

		switch(ans){
		
			case 0:
				System.out.println(num +" is an Even Number.");
				break;
			default :
				System.out.println(num +" is an Odd Number.");
		}
	}
}
