/*
Q9.ifelse and switch case
Write a program in which students should enter marks for 5 different subjects. If
all subjects have the above passing marks add them and provide a switch case to
print grades(first class with distinction, first class, second class), if a student gets a
fail in any subject program should print “You failed the exam”.
*/

class Student{

	public static void main(String n[]){
	
		int s1 = 44;
		int s2 = 53;
		int s3 = 40;
		int s4 = 80;
		int s5 = 90;
		
		int sum = s1+s2+s3+s4+s5;
		int per = (sum/5)*100;

		if(s1>=40 && s2>=40 && s3>=40 && s4>=40 && s5>=40){
			

			switch(per){
			
				case 85:
					System.out.println("First class with Distinction");
					break;

				case 75:
                                        System.out.println("First class");
                                        break;

				case 60:
                                        System.out.println("Second class");
                                        break;

				case 50:
                                        System.out.println("Third class");
                                        break;

				default:
                                        System.out.println("pass");
			}
			
		} else {
		
			System.out.println("Your are Failed in Exam.");
		}
	
	}
}
