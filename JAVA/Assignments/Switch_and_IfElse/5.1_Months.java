/*If Else
 Q5.
Write a program to print the month name according to the month number.
Input : 4
Output : April
Input : 9
Output : September
*/

class Months{

	public static void main(String n[]){
	
		int monthNum = 13;

		if(monthNum == 1){
		
			System.out.println(monthNum +"-> January");	
		} else if (monthNum == 2){

                        System.out.println(monthNum +"-> February");
                } else if(monthNum == 3){

                        System.out.println(monthNum +"-> March");
                } else if(monthNum == 4){

                        System.out.println(monthNum +"-> April");
                } else if(monthNum == 5){

                        System.out.println(monthNum +"-> May");
                } else if(monthNum == 6){

                        System.out.println(monthNum +"-> June");
                } else if(monthNum == 7){

                        System.out.println(monthNum +"-> July");
                } else if(monthNum == 8){

                        System.out.println(monthNum +"-> August");
                } else if(monthNum == 9){

                        System.out.println(monthNum +"-> September");
                } else if(monthNum == 10){

                        System.out.println(monthNum +"-> Octomber");
                } else if(monthNum == 11){

                        System.out.println(monthNum +"-> November");
                } else if(monthNum == 12){

                        System.out.println(monthNum +"-> December");
                } else {
		
			System.out.println("Enter the Valid Month Number between 1 to 12..... ");
		}

	}
}
