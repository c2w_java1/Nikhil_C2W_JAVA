/*
Q8 Write a program to print the following pattern
Number of rows = 4
d c b a
d c b a
d c b a
d c b a
Number of rows = 3
c b a
c b a
c b a
*/

import java.util.*;

class Reverse{


	public static void main(String n[]){
		
		Scanner sc = new Scanner(System.in);
		System.out.print("number of row & column : ");
		int row  = sc.nextInt();


		for (int i=1; i<=row; i++){
		    	
			char ch = 'a';
			ch+=row;

			for(int j=1; j<=row; j++){
				
				System.out.print(--ch +"   ");
			}
			System.out.println("\n");
		}


	}

}
