/*
 * Q1 Write a program to print the following pattern.
Number of rows = 3
$# $# $#
$# $# $#
$# $# $#
Number of rows = 4
$# $# $# $#
$# $# $# $#
$# $# $# $#
$# $# $# $#
*/

import  java.util.*;

class Pattern{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("How Many Row : ");
		int row = sc.nextInt();

		for (int i = 1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
			
				System.out.print(" $#  ");
			}
			System.out.println("\n");
		}
	}
} 
