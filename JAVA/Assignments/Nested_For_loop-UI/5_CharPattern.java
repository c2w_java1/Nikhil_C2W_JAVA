/*
Q5 Write a program to print the following pattern
Number of rows = 3
A C E
G I K
M O Q
*/


import java.util.*;

class CharPattern{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("How Many Row & Column : ");
		int row = sc.nextInt();

		char ch = 'A';

		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
			
				System.out.print(ch +"	  ");
				ch+=2;
			}

			System.out.println("\n");
		}
	}
}
