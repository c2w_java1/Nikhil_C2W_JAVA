/*
Q9 Write a program to print the following pattern
Number of rows = 3
C1 C2 C3
C4 C5 C6
C7 C8 C9
Number of rows = 4
D1 D2 D3 D4
D5 D6 D7 D8
D9 D10 D11 D12
D13 D14 D15 D16
*/


import java.util.*;

class NumChar{

	public static void main(String n[]){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Number of Row & Coloumn : ");
		int row = sc.nextInt();

		int num = 1;
		char ch = 'A';
		ch+=row-1;

		for(int i=1; i<=row; i++){
		
			for(int j=1; j<=row; j++){
			
				System.out.print(ch +"" +num +"   " );
				num++;
			}
			System.out.println("\n");
		}
	}
}
