/*
3. A scientist is working on his research and wants to find an area using
pi. (Write a code to print the value of pi angle of triangle).Scientists are
also eager to print the highest degree of angle of circle(360 degree).
*/

class Scientist{

	public static void main(String n[]){
	
		float pi = 3.14f;
		int angle = 360;

		System.out.println("Pi - " +pi);
		System.out.println("Angle  - " +angle);
	}
}
