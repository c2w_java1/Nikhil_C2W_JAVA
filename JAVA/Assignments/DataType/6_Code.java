/*
 6. A person is storing date, month and year in variables (Write a code to
print the date, month, year, and also print the total seconds in a day,
month and year.)
*/

class Data{

	public static void main(String n[]){
	
		int date = 28;
		int month = 01;
		int year = 2003;

		int secondsInDay = 86400;
		double secondsInMonth = 2628002.88d;
		long secondsInYear =31536000l;

		System.out.println("Date - " +date +" / " +month +" / " +year);
		System.out.println("Seconds in Day = " +secondsInDay);
		System.out.println("Seconds in Month = " +secondsInMonth);
		System.out.println("Seconds in Year = " +secondsInYear);
	}
}
