/*
9. Write a program to print the temperature of the Air Conditioner in
degrees and also print the standard room temperature using appropriate
data types.
*/

class Temp{

	public static void main(String n[]){
	
		float temp = 25.78f;
		float t = 20.00f;

		System.out.println("Temperature = "+temp);
		System.out.println("Normal Room Temprature = "+t);

	}
}
